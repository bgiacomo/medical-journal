package com.sdm.medicaljournal;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View v){

        Intent i=null;

        System.out.println("ciao");

        switch (v.getId()){

            //prova commento


            case R.id.datos_personales:
                i=new Intent(this,DatosActivity.class);
                break;
            case R.id.tratamiento:
                i=new Intent(this,TratamientoActivity.class);
                break;
            case R.id.hospitales:
                i=new Intent(this,HospitalesActivity.class);
                break;
            case R.id.visitas:
                i=new Intent(this,VisitasActivity.class);
                break;
            case R.id.noticias:
                i=new Intent(this,PreferenciasActivity.class);
                break;
            case R.id.emergencias:
                i=new Intent(this,EmergenciasActivity.class);
                break;
            default:
                break;

        }

        startActivity(i);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
