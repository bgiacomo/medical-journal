package com.sdm.medicaljournal;

/**
 * Created by Giacomo on 04/05/15.
 */
public class ClassicSingleton {
    private static ClassicSingleton instance = null;
    private String pref;



    protected ClassicSingleton(String s) {
        pref=s;
        // Exists only to defeat instantiation.
    }
    public static ClassicSingleton getInstance(String p) {
        if(instance == null) {
            instance = new ClassicSingleton(p);
        }
        return instance;
    }

    public String getPref() {
        return pref;
    }
}
