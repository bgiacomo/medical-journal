package com.sdm.medicaljournal;

import android.content.Context;

/**
 * Created by Giacomo on 04/05/15.
 */
public class ParametersForNews {

    private String s;
    private Context mc;

    public ParametersForNews(String s, Context mc) {
        this.s = s;
        this.mc = mc;
    }

    public String getS() {
        return s;
    }

    public Context getMc() {
        return mc;
    }
}
