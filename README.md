# README #

Medical Journal is an Android app developed for the course of Mobile Programming of the UPV, Politechnical University of Valencia.

[Info about the course](https://www.upv.es/pls/oalu/sic_asi.Busca_Asi?p_codi=7179&p_caca=2016&P_IDIOMA=i&p_vista=)

### Developers ###

* Remigio Romano
* Mariacristina Dominicis
* Hector Ramiro Serrano
* Giacomo Bartoli

### Project ###

If you're interested in this project please contact one of the developers.