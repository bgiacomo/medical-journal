package com.sdm.medicaljournal;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;


public class PreferenciasActivity extends ActionBarActivity {

    private String selectedPref;
    RadioButton nut;
    RadioButton dep;
    RadioButton enf;
    RadioButton med;
    RadioButton san;
    RadioButton emb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferencias);

        nut=(RadioButton)findViewById(R.id.nut);
        dep=(RadioButton)findViewById(R.id.dep);
        enf=(RadioButton)findViewById(R.id.enf);
        med=(RadioButton)findViewById(R.id.med);
        san=(RadioButton)findViewById(R.id.san);
        emb=(RadioButton)findViewById(R.id.emb);

        setListener();

        Button btn=(Button)findViewById(R.id.conf);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.d("Medical journal: ",selectedPref);

                if(selectedPref==null){
                    Log.d("errore","string nulla");
                 dialogError();
                }else {

                    Intent i = new Intent(getApplicationContext(), NoticiasActivity.class);
                    i.putExtra("pref",selectedPref);
                    startActivity(i);
                }
            }
        });

    }

    public void dialogError(){
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
        dlgAlert.setMessage("You should select almost 1 category!");
        dlgAlert.setTitle("Medical Journal");
        dlgAlert.setPositiveButton("OK", null);
        dlgAlert.setCancelable(true);
        dlgAlert.create().show();
    }

    public void setListener(){

        nut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(nut.isChecked()){
                    selectedPref="nut";
                    Log.d("Pref: ",selectedPref);
                }
            }
        });

        dep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dep.isChecked()){
                    selectedPref="dep";
                    Log.d("Pref: ",selectedPref);
                }
            }
        });
        enf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(enf.isChecked()){
                    selectedPref="enf";
                    Log.d("Pref: ",selectedPref);
                }
            }
        });
        med.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(med.isChecked()){
                    selectedPref="med";
                    Log.d("Pref: ",selectedPref);
                }
            }
        });

        san.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(san.isChecked()){
                    selectedPref="san";
                    Log.d("Pref: ",selectedPref);
                }
            }
        });
        emb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(emb.isChecked()){
                    selectedPref="emb";
                    Log.d("Pref: ",selectedPref);
                }
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_preferencias, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
