package com.sdm.medicaljournal;

/**
 * Created by Giacomo on 05/05/15.
 */
public class Visita_Model {

    private String name;
    private String place;
    private int year;
    private int month;
    private int dayOfMonth;
    private int hour;
    private int minute;


    public Visita_Model(int year, int month, int dayOfMonth,String s,String p,int h,int m) {
        this.year = year;
        this.month = month;
        this.dayOfMonth = dayOfMonth;
        this.name=s;
        this.place=p;
        this.hour=h;
        this.minute=m;

    }

    public String getName() {
        return name;
    }

    public String getPlace() {
        return place;
    }

    public int getYear() {
        return year;
    }



    public int getMonth() {
        return month;
    }



    public int getDayOfMonth() {
        return dayOfMonth;
    }


    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }


}
