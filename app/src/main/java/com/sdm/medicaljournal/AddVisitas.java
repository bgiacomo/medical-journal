package com.sdm.medicaljournal;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;


public class AddVisitas extends ActionBarActivity {

    private String nombre;
    private String lugar;

    private int monthTemp;
    private int yearTemp;
    private int dayOfMonthTemp;
    private int hour;
    private int minute;
    private boolean isEditing=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_visitas);

        Intent i=getIntent();
        this.monthTemp=i.getIntExtra("month", -1);
        this.yearTemp=i.getIntExtra("year", -1);
        this.dayOfMonthTemp=i.getIntExtra("dayOfMonth", -1);
        TextView data=(TextView)findViewById(R.id.textView4);
        data.setText(dayOfMonthTemp + "/" + (monthTemp + 1) + "/" + yearTemp);

        isEditing=i.getBooleanExtra("isEditing",false);
        Log.d("EDITING",""+isEditing);



        if(i.getStringExtra("name")!=null){
            EditText etNombre=(EditText)findViewById(R.id.nombreVisita);
            EditText etLugar=(EditText)findViewById(R.id.nombreLugar);

            etNombre.setText(i.getStringExtra("name"));
            etLugar.setText(i.getStringExtra("place"));

            //QUANDO CONFERMO AGGIUNGERE CODICE PER AGGIORNAMENTO!
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_visitas, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_confirm) {

            if(checkInputUser()){

                Log.d("APP: ","aggiungo nuovo evento");
                EditText etNombre=(EditText)findViewById(R.id.nombreVisita);
                EditText etLugar=(EditText)findViewById(R.id.nombreLugar);
                TimePicker tp=(TimePicker)findViewById(R.id.timePicker);

                Visita_Model vm=new Visita_Model(yearTemp,monthTemp,dayOfMonthTemp,etNombre.getText().toString(),etLugar.getText().toString(),tp.getCurrentHour(),tp.getCurrentMinute());
                storeEventIntoDB(vm);
                Toast.makeText(getApplicationContext(), "New event added!", Toast.LENGTH_LONG).show();
                super.onBackPressed();

            }else{
                final AlertDialog alertDialog = new AlertDialog.Builder(
                        AddVisitas.this).create();
                alertDialog.setTitle("Attention");
                alertDialog.setMessage("Please fill all the fields!");
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });

                alertDialog.show();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void storeEventIntoDB(Visita_Model vm){

        SQLiteDatabase db=this.openOrCreateDatabase("eventsDB",MODE_PRIVATE,null);

        if(!isEditing) {
            Log.d("APP: ","sto aggiungendo l'evento..");
            try {
                db.beginTransaction();
                db.execSQL("CREATE TABLE IF NOT EXISTS Events(Name VARCHAR,Place VARCHAR,Month INTEGER,Year INTEGER,DayOfMonth INTEGER,Hour INTEGER,Minute INTEGER);");
                String query = "INSERT INTO Events VALUES('" + vm.getName() + "','" + vm.getPlace() + "','" + vm.getMonth() + "','" + vm.getYear() + "','" + vm.getDayOfMonth() + "','" + vm.getHour() + "','" + vm.getMinute() + "');";
                Log.d("QUERY: ", query);
                db.execSQL(query);
                db.setTransactionSuccessful();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT)
                        .show();

            } finally {
                db.endTransaction();
            }
        }else{
            Log.d("APP: ","sto modificando l'evento..");
            try {
                db.beginTransaction();

                db.execSQL("UPDATE Events SET Place='"+vm.getPlace()+"' WHERE Name='"+vm.getName()+"'");

                db.setTransactionSuccessful();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT)
                        .show();

            } finally {
                db.endTransaction();
            }

        }

    }



    public boolean checkInputUser(){

        EditText etNombre=(EditText)findViewById(R.id.nombreVisita);
        EditText etLugar=(EditText)findViewById(R.id.nombreLugar);

        if(etNombre.getText().toString().isEmpty() || etLugar.getText().toString().isEmpty()){
            return false;
        }else {
            return true;
        }

    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }


}
