package com.sdm.medicaljournal;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Giacomo on 04/05/15.
 */
class MySimpleArrayAdapter extends BaseAdapter {

    Context context;
    ArrayList<News_Model> data;
    private static LayoutInflater inflater = null;

    public MySimpleArrayAdapter(Context context, ArrayList<News_Model> data) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.noticias, null);
        TextView text = (TextView) vi.findViewById(R.id.desc);
        TextView text2 = (TextView) vi.findViewById(R.id.title);
        text2.setText(data.get(position).getDescription());
        text.setText(data.get(position).getTitle());
        return vi;
    }
}
