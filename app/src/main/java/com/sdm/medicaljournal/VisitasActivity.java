package com.sdm.medicaljournal;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.CalendarView.OnDateChangeListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemLongClickListener;

import java.util.ArrayList;

public class VisitasActivity extends ActionBarActivity {


    private String[] array = {"Visitas 1", "Visitas 2", "Visitas 3", "Visitas 4"};

    private ListView visitasListView;
    private ArrayAdapter arrayAdapter;
    private Long date;
    private CalendarView cv;
    public int year2;
    public int month2;
    public int dayOfMonth2;
    private Visita_Model vmTemp;
    private boolean shortListenerActive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitas);

        shortListenerActive=true;

        visitasListView = (ListView) findViewById(R.id.listView);
        cv = (CalendarView) findViewById(R.id.calendarView);
        date = cv.getDate();


        //listener por la data

        final ArrayList<String> prova = new ArrayList<String>();

        cv.setOnDateChangeListener(new OnDateChangeListener() {
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                if (cv.getDate() != date) {

                    date = cv.getDate();
                    year2 = year;
                    month2 = month;
                    dayOfMonth2 = dayOfMonth;
                    //Toast.makeText(view.getContext(), "Year=" + year + " Month=" + month + " Day=" + dayOfMonth, Toast.LENGTH_LONG).show();
                    prova.clear();
                    prova.addAll(getEvents(month, year, dayOfMonth));

                    if (prova.isEmpty()) {
                        //do nothing
                        Log.d("MJ:", "ARRAY VUOTO!");
                        ArrayList<String> emptyArray = new ArrayList<>();
                        emptyArray.add("No events today");
                        refreshListEvents(emptyArray);
                    } else {
                        System.out.println("ARRAYPROVA: " + prova.toString());
                        refreshListEvents(prova);
                    }
                }
            }
        });
        prova.clear();

        visitasListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView tv = (TextView) findViewById(R.id.event);
                Log.d("CLICK: ", tv.getText().toString());
                if (tv.getText().toString() != "No events today" && shortListenerActive==true) {
                    showAlertEditing("Do you really want to edit your event?");
                } else {
                    //do nothing
                }
            }
        });

        visitasListView.setOnItemLongClickListener(new OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView parent, View view, int position, long id) {
                Log.d("APP:","long preeesss");
                shortListenerActive=false;
                showAlertDelete("Do you want to delete this event?");
                return false;
            }
        });
    }

    public void showAlertDelete(String s){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                VisitasActivity.this);

        // set title
        alertDialogBuilder.setTitle("Attention");

        // set dialog message
        alertDialogBuilder
                .setMessage(s)
                .setCancelable(false)
                .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, close
                        // current activity
                        TextView tv=(TextView)findViewById(R.id.event);
                        Log.d("CLICK: ", tv.getText().toString());
                        shortListenerActive=!shortListenerActive;
                        if(tv.getText().toString()!="No events today") {

                            //CODICE QUERY PER ELIMINARE EVENTO
                            deleteEvent();

                            dialog.dismiss();
                        }
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        shortListenerActive = !shortListenerActive;
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


    public void showAlertEditing(String s){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                VisitasActivity.this);

        // set title
        alertDialogBuilder.setTitle("Attention");

        // set dialog message
        alertDialogBuilder
                .setMessage(s)
                .setCancelable(false)
                .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, close
                        // current activity
                        TextView tv=(TextView)findViewById(R.id.event);
                        Log.d("CLICK: ", tv.getText().toString());
                        if(tv.getText().toString()!="No events today") {
                            Intent i = new Intent(getApplicationContext(), AddVisitas.class);
                            i.putExtra("month", vmTemp.getMonth());
                            i.putExtra("year", vmTemp.getYear());
                            i.putExtra("dayOfMonth", vmTemp.getDayOfMonth());
                            i.putExtra("name",vmTemp.getName());
                            i.putExtra("place",vmTemp.getPlace());
                            i.putExtra("isEditing",true);
                            startActivity(i);
                            dialog.dismiss();
                        }
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void deleteEvent(){

        SQLiteDatabase db=this.openOrCreateDatabase("eventsDB",MODE_PRIVATE,null);


        try {
            db.beginTransaction();

            db.delete("Events", "Place = ?", new String[] { vmTemp.getPlace() });


            db.setTransactionSuccessful();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT)
                    .show();

        } finally {
            db.endTransaction();
            db.close();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_visitas, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add) {
            Intent i = new Intent(this.getApplicationContext(), AddVisitas.class);
            i.putExtra("month", this.month2);
            i.putExtra("year", this.year2);
            i.putExtra("dayOfMonth", this.dayOfMonth2);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public ArrayList<String> getEvents(int m, int y, int dom) {

        SQLiteDatabase db = this.openOrCreateDatabase("eventsDB", MODE_PRIVATE, null);

        ArrayList<String> dailyEvents = new ArrayList<>();

        try {


            String s = "";
            Cursor c1 = db.rawQuery("SELECT * FROM Events WHERE Month = ? AND Year = ? AND DayOfMonth = ?", new String[]{"" + m, "" + y, "" + dom});
            Log.d("QUERY: ", "" + db.rawQuery("SELECT * FROM Events WHERE Month = ? AND Year = ? AND DayOfMonth = ?", new String[]{"" + m, "" + y, "" + dom}));
            while (c1.moveToNext()) {

                s = c1.getString(c1.getColumnIndex("Name")) + " at " + c1.getString(c1.getColumnIndex("Place")) + " - " + c1.getString(c1.getColumnIndex("Hour")) + ":" + c1.getString(c1.getColumnIndex("Minute"));
                Log.d("DB:", s);
                vmTemp=new Visita_Model(Integer.parseInt(c1.getString(c1.getColumnIndex("Year"))),Integer.parseInt(c1.getString(c1.getColumnIndex("Month"))),Integer.parseInt(c1.getString(c1.getColumnIndex("DayOfMonth"))),c1.getString(c1.getColumnIndex("Name")),c1.getString(c1.getColumnIndex("Place")),Integer.parseInt(c1.getString(c1.getColumnIndex("Hour"))),Integer.parseInt(c1.getString(c1.getColumnIndex("Minute"))) );

                dailyEvents.add(s);

            }
            c1.close();
            db.close();


        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "No data to show", Toast.LENGTH_SHORT)
                    .show();

        } finally {
        }

        return dailyEvents;

    }

    public void clearList(){

    }

    public void refreshListEvents(ArrayList<String> events){

        visitasListView = (ListView) findViewById(R.id.listView);
        arrayAdapter = new ArrayAdapter<String>(this, R.layout.event,R.id.event, events);
        visitasListView.setAdapter(arrayAdapter);

    }
}
