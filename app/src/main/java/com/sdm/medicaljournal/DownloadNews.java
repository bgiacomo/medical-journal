package com.sdm.medicaljournal;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;



/**
 * Created by Giacomo on 29/04/15.
 */
public class DownloadNews extends AsyncTask<ParametersForNews,Void,ArrayList<News_Model>>{

    private ArrayList<News_Model> array=new ArrayList<>();
    private String url;
    ProgressDialog dialog;
    Context myContext;

    // XML node keys
    static final String KEY_ITEM = "item"; // parent node
    static final String KEY_NAME = "title";
    static final String KEY_LINK = "link";
    static final String KEY_DESC = "description";


    @Override
    protected ArrayList<News_Model> doInBackground(ParametersForNews... params) {

        String s=params[0].getS();
        this.myContext=params[0].getMc();


           System.out.println(s);
            switch (s){


                case "nut":
                    url = "http://www.medpagetoday.com/rss/nursing.xml";
                    break;
                case "dep":
                    url = "http://www.medpagetoday.com/rss/Cardiology.xml";
                    break;
                case "enf":
                    url = "http://www.medpagetoday.com/rss/InfectiousDisease.xml";
                    break;
                case "med":
                    url = "http://www.medpagetoday.com/rss/PrimaryCare.xml";
                    break;
                case "san":
                    url = "http://www.medpagetoday.com/rss/Headlines.xml";
                    break;
                case "emb":
                    url = "http://www.medpagetoday.com/rss/Pediatrics.xml";
                    break;

            }

        XMLParser parser = new XMLParser();
        String xml = parser.getXmlFromUrl(url); // getting XML
        Document doc = parser.getDomElement(xml); // getting DOM element

        NodeList nl = doc.getElementsByTagName(KEY_ITEM);

        // looping through all item nodes <item>
        for (int i = 0; i < nl.getLength(); i++) {
            Element e = (Element) nl.item(i);
            String title = parser.getValue(e, KEY_NAME); // name child value
            String link = parser.getValue(e, KEY_LINK); // link child value
            String description = parser.getValue(e, KEY_DESC); // description child value

            News_Model nm=new News_Model(title,description,link);
            array.add(nm);
        }


        return array;
    }



}
