package com.sdm.medicaljournal;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;


public class NoticiasActivity extends ActionBarActivity implements Serializable{

    private String prefCat;

    private ArrayList<News_Model> array=new ArrayList<>();
    private XmlPullParserFactory xmlFactoryObject = null;
    private XmlPullParser myparser=null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noticias);

        try {
            prefCat=getIntent().getExtras().getString("pref");

            downloadDatas();


        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        Log.d("Notizie, carico..", prefCat);

        ListView newsList=(ListView)findViewById(R.id.listNoticias);



        MySimpleArrayAdapter msaa=new MySimpleArrayAdapter(this.getApplicationContext(),array);
        newsList.setAdapter(msaa);

        newsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("CLICCATO NUMERO: ", "" + position);
                Intent i = new Intent(getApplicationContext(), NewsReader.class);
                i.putExtra("url", array.get(position).getUrl());
                startActivity(i);
            }
        });


    }

    public void refreshListWithOfflineNews(){

        ListView newsList=(ListView)findViewById(R.id.listNoticias);


        MySimpleArrayAdapter msaa=new MySimpleArrayAdapter(this.getApplicationContext(),array);
        newsList.setAdapter(msaa);

        newsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("CLICCATO NUMERO: ", "" + position);
                Intent i = new Intent(getApplicationContext(), NewsReader.class);
                i.putExtra("url", array.get(position).getUrl());
                startActivity(i);
            }
        });

    }


    public void downloadDatas() throws ExecutionException, InterruptedException {

        ParametersForNews pn=new ParametersForNews(prefCat,this.getApplicationContext());

        Log.d("BOOL",""+isNetworkAvailable());

        if(isNetworkAvailable()) {
            array = new DownloadNews().execute(pn).get();
            savaDataOffline();


        }else {
            //caricaDialog();
            AlertDialog alertDialog = new AlertDialog.Builder(NoticiasActivity.this).create();
            alertDialog.setTitle("Attention");
            alertDialog.setMessage("No internet connection. You are going to read offline news.");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                            grabDataOffline();

                        }
                    });
            alertDialog.show();
        }
        System.out.println(array.toString());

    }


    @Override
    public void onBackPressed() {
        Intent i=new Intent(getApplicationContext(),MainActivity.class);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_noticias, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void savaDataOffline() {
        try {
            FileOutputStream fos = getApplicationContext().openFileOutput("cacheNews.dat", Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(array);

            fos.close();
            os.close();
        } catch (Exception e) {
            e.toString();
        }
    }

    public void grabDataOffline(){

            try{

                Log.d("carico i dati offline","aspetta..");

                FileInputStream fin;
                ObjectInputStream ois=null;
                fin = getApplicationContext().openFileInput("cacheNews.dat");
                ois = new ObjectInputStream(fin);
                array = (ArrayList<News_Model>) ois.readObject();
                ois.close();

                System.out.println("ARRAY OFFLINE: " + array.toString());
                refreshListWithOfflineNews();

            }catch (Exception e){
                Log.d("ECCEZZIONEEEEEEE",e.toString());
                e.toString();
            }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }


}
